/**
 * Created by downtime on 2017-03-30.
 */
import React from 'react'
const Triangle = (props) => {
	var marginLeft;
	if(props.pos == true) {
		marginLeft = 0;
	}
	else{
		marginLeft = 40
	}
	return (
		<div
			style={{content:"", /* triangle */
				position:'absolute',
				bottom:'-10px', /* value = - border-top-width - border-bottom-width */
				left: '50px', /* controls horizontal position */
				marginLeft: `${marginLeft}%`,
				borderWidth: '10px 10px 0', /* vary these values to change the angle of the vertex */
				borderStyle: 'solid',
				borderColor: `${props.color} transparent`}} />
	)
}
export default Triangle
