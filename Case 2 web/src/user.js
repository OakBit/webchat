/**
 * Created by downtime on 2017-03-23.
 */
import React from 'react'
import ReactDOM from 'react-dom'
import ListItem from 'material-ui/List/ListItem'
class User extends React.Component {
    constructor() {
        super()
    }
    componentDidMount = () => {
        var messageDOM = ReactDOM.findDOMNode(this)
        messageDOM.scrollIntoView({block: "end", behavior: "smooth"})
        messageDOM.blur()
    }
    render() {
        var user = this.props.user
        return (
            <ListItem ref='user' style={{textAlign: 'center'}} disabled={true}>
                <div>
                    <span style={{fontWeight:'bold', color: user.colour }}>{user.name}</span>
                </div>
            </ListItem>
        )
    }
}
export default User
