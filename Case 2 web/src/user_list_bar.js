import React from 'react'; // pulls Component object out of React library
import PersonIcon from 'material-ui/svg-icons/Action/accessibility'
import {Toolbar, ToolbarTitle} from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
const TopBar = (props) => {
	const iconStyles = {
			height: 50,
			width: 50,
			marginTop: -10
		},
		onIconClicked = () => {
			props.viewDialog(); // notify the parent
		}
	return (
		<Toolbar style={{backgroundColor: '#00897B', color: 'white', marginBottom:20,textAlign: 'center'}}>
			<ToolbarTitle text='Chat App'/>
			<IconButton tooltip="User list"
									tooltipPosition="bottom-left"
									onClick={onIconClicked}
									iconStyle={iconStyles}
			>
				<PersonIcon style={iconStyles} color='white' />
			</IconButton>
		</Toolbar>
	)
}
export default TopBar
