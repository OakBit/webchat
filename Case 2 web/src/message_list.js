/**
 * Created by downtime on 2017-03-23.
 */
import React from 'react'
import List from 'material-ui/List/List'
import Message from './message'
const MessageList = (props) => {
    const messages = props.messages.map((message, index) => {
        return (<Message key={index} msg={message}/>);
    })
    return (
        <List className='list'>
            {messages}
        </List>
    )
}
export default MessageList
