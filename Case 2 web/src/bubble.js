/**
 * Created by downtime on 2017-03-30.
 */
import React from 'react'
import styles from './index.scss'
const Bubble = (props) => {
	var bubbleStyle = ''
	if(props.message.isMine){
		bubbleStyle = 'userBubble'
	}
	else{
		bubbleStyle = 'userBubbleRight'
	}
	return (
		<div className={bubbleStyle} style={{backgroundColor: props.message.colour, color:'white'}}>
			<span style={{fontWeight:'bold'}}>{props.message.user} says:</span>
			<br />
			<br />
			<span style={{display:'block',wordWrap:'break-word'}}>{props.message.msg}</span>
			<br />
			<br />
			<span style={{fontSize: 'smaller'}}>{props.message.time}</span>
		</div>
	)
}
export default Bubble
