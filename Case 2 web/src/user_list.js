/**
 * Created by downtime on 2017-04-13.
 */
/**
 * Created by downtime on 2017-03-23.
 */
import React from 'react'
import List from 'material-ui/List/List'
import User from './user'
const UserList = (props) => {
	const users = props.users.map((user) => {
		return (<User key={user.id} user={user}/>);
	})
	return (
		<List className='list'>
			{users}
		</List>
	)
}
export default UserList
