
import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {Card, CardHeader,CardActions, CardText} from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import MessageList from './message_list'
import UserList from './user_list'
import TopBar from './user_list_bar'

import Message from './message'

import styles from './index.scss'

import io from 'socket.io-client'
//var socket = io('http://localhost:3020') //testing
var socket = io()

class ChatComponent extends React.Component {
	constructor() {
		super()
		this.state = {
			users: [],
			userName: '',
			isJoined: false,
			isTyping:false,
			whoIsTyping: '',
			messages:[],
			message:'',
			dialogOpen: false,
			error: ''
		}
	}

	componentDidMount() {
		socket.on('server message', this.MessageReceived)
		socket.on('connection status', this.JoinResponse)
		socket.on('typing status', this.TypingStatus)
	}

	Join = () => {
		socket.emit('join', this.state.userName)
	}

	JoinResponse = (res) => {
		if(res.successful) {
			this.setState({error:''})
			this.setState({isJoined: true})
		}
		else{
			this.setState({error:res.statusMessage})
		}
	}

	MessageReceived = (msg) => {
		var isMyMessage = msg.user === this.state.userName

		this.setState({users:msg.users})

		msg.isMine = isMyMessage
		this.state.messages.push( msg )
		//To make components update
		this.forceUpdate()
	}

	TypingStatus = (msg)=>{
		console.log(msg)
		this.setState({whoIsTyping:msg})
	}

	handleChangeUserName = (event, value) => {
		this.setState({userName: value})
	}
	handleMessage = (event) => {
		event.preventDefault()
		if(this.state.message.length > 0) {
			socket.emit('chat message', this.state.message)

			this.setState({isTyping: false})
			var txt = document.getElementById('messageTxt')
			txt.value = ''
		}
	}

	handleTyping = (event, text) => {
		//Only send the message once
		if(!this.state.isTyping){
			socket.emit('typing status')
			this.setState({isTyping:true})
		}
		//A bit redundant
		this.setState({message:text})
	}

	handleOpenDialog = () => {
		this.setState({dialogOpen: true});
	}
	handleCloseDialog = () => {
		this.setState({dialogOpen: false});
	}

	render() {
		return (
			<MuiThemeProvider>
				<div>
					<TopBar viewDialog={this.handleOpenDialog} style={{position:'fixed', top:'0'}}/>
					<Dialog title="Current Users" style={{textAlign:'center'}} modal={false} open={this.state.dialogOpen} onRequestClose={this.handleCloseDialog}>
						<UserList id="userList" users={this.state.users}/>
					</Dialog>

					{this.state.isJoined === false &&
					<Card style={{textAlign: 'center'}}>

						<div>
							<TextField name="userNameTxt" hintText="name" onChange={this.handleChangeUserName} errorText={this.state.error}/>
						</div>
						<form>
							<Button type="button" label="Join Chat" style={{margin:10}} disabled={this.state.userName.length < 1} onTouchTap={this.Join}/>
						</form>
					</Card>
					}
					{this.state.isJoined === true &&
					<Card style={{textAlign: 'center'}}>

						<div>{this.state.whoIsTyping}</div>
						<div>
							<MessageList id="messageList" messages={this.state.messages}/>
						</div>
						<CardActions>
							<div >
								<form name="messageForm"  onSubmit={this.handleMessage}>
									<TextField id="messageTxt" hintText="message" style={{width:'100%'}} onChange={this.handleTyping} errorText={this.state.error}/>
								</form>
							</div>
						</CardActions>
					</Card>
					}
				</div>
			</MuiThemeProvider>
		)
	}
}
export default ChatComponent
