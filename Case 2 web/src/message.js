
/**
 * Created by downtime on 2017-03-23.
 */
import React from 'react'
import ReactDOM from 'react-dom'
import ListItem from 'material-ui/List/ListItem'
import Bubble from './bubble'
import Triangle from './triangle'
class Message extends React.Component {
	constructor() {
		super()
	}
	componentDidMount = () => {
		var messageDOM = ReactDOM.findDOMNode(this)
		messageDOM.scrollIntoView({block: "end", behavior: "smooth"})
		messageDOM.blur()
	}
	render() {
		var currentMessage = this.props.msg
		return (
			<ListItem ref='message' style={{textAlign: 'left', colour: currentMessage.colour}} disabled={true}>
				<Bubble message={currentMessage}/>
				<Triangle color={currentMessage.colour} pos={currentMessage.isMine}/>
			</ListItem>
		)
	}
}
export default Message
