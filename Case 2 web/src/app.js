import React from 'react';
import ReactDOM from 'react-dom'
import ChatComponent from './chat'
// Needed for onTouchTap
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()
ReactDOM.render(
    <ChatComponent />, document.getElementById('app')
)

